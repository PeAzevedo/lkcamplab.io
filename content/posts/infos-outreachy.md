---
title: "Informações importantes - aplicação inicial do Outreachy"
date: 2022-08-22
draft: false
tags: ["Outreachy"]
categories: ["Informações"]
authors: ["Isabella Breder"]
---

Aqui estão algumas informações importantes acerca da aplicação inicial do Outreachy, além de dicas sobre o preenchimento de algumas partes. As inforamações aqui são voltadas para quem é aluno da Unicamp. Caso não seja, preencha de acordo com suas especificações!

<!-- vim-markdown-toc GFM -->

* [Informações gerais](#informações-gerais)
    * [Prazos e datas](#prazos-e-datas)
    * [Outros adendos](#outros-adendos)
* [A aplicação inicial](#a-aplicação-inicial)
    * [No que consiste](#no-que-consiste)
    * [Redação](#redação)
    * [Sobre a universidade e períodos](#sobre-a-universidade-e-períodos)

<!-- vim-markdown-toc -->

# Informações gerais

## Prazos e datas

- A aplicação inicial vai até o dia **29 de agosto às 13:00** do horário de Brasília (próxima segunda-feira), então se atentem ao prazo
- O estágio acontecerá de **5 de Dezembro/2022 a 3 de Março/2023** para quem for aprovado
- A lista de projetos estará finalizada até **1º de Outubro**

## Outros adendos

Como informado anteriormente, é necessário **inglês avançado**/fluência, no entanto qualquer dúvida que tiverem quanto ao preenchimento da aplicação inicial é só nos contatar que vamos ajudá-los, inclusive nas respostas dissertativas.

{{< alerts error
"Após iniciar a aplicação inicial **NÃO** é possível salvá-la e continuar de onde parou. Caso saia da aplicação, terá de começar novamente, por isso recomendo pensar antes principalmente na resposta das questões dissertativas, e abrir a aplicação somente após escritas e revisadas. O preenchimento de todos os passos não é rápida, então separe pelo menos **1 hora ininterrupta** para tal."
>}}

---

# A aplicação inicial

## No que consiste

Essa aplicação inicial consiste em **criar uma conta** no site do Outreachy e preencher um **questionário de elegibilidade**, já que o programa é voltado para pessoas sub-representadas no mercado tecnológico.

## Redação

Uma das etapas do questionário de elegibilidade é responder a algumas **perguntas dissertativas**, com limite de 1000 caracteres (aproximadamente 200 palavras) cada. As perguntas são:
1. Are you part of an underrepresented group (in the technology industry of the country listed above)? How are you underrepresented?
2. What systemic bias or discrimination would you face if you applied for a job in the technology industry of your country?
3. Does your learning environment have few people who share your identity or background? Please provide details. 
4. What systemic bias or discrimination have you faced while building your skills? 
{{< alerts info
"OBS.: há ainda uma caixa de texto opcional na qual você pode adicionar possíveis avisos de gatilhos contidos em suas respostas."
>}}

## Sobre a universidade e períodos

Na parte que pergunta sobre a universidade e seu semestre atual/os próximos (steps 7 e 8), preencha da seguinte maneira:
1. **University name:** Universidade Estadual de Campinas (Unicamp)
2. **University website:** [www.unicamp.br/unicamp/](www.unicamp.br/unicamp/)
3. **Link to your official academic calendar for you current school term:** [www.dac.unicamp.br/portal/calendario/2022/graduacao/alunos](www.dac.unicamp.br/portal/calendario/2022/graduacao/alunos)
4. **Link to you official academic calendar for you next school term:** [www.dac.unicamp.br/portal/calendario/2022/graduacao/alunos](www.dac.unicamp.br/portal/calendario/2022/graduacao/alunos)
5. **Degree name:** {o nome do seu curso, em inglês}
6. **Current term:** {insira o número do seu semestre aqui)th semester
7. **Date classes start for your current term:** 08/15/2022
8. **Date exams end for your current term:** 12/21/2022
9. Caso seja seu último semestre na universidade, marque a caixa que contém **This is the last term in my degree.**
10. **Next term name:** {insira o número do próximo semestre que irá cursar aqui}th semester
11. **Date classes start for your next term:** 03/02/2023
12. **Date exams end for your next term:** 07/21/2023
13. Caso o próximo semestre que irá cursar seja seu último semestre na universidade, marque a caixa que contém **This is the last term in my degree**
14.  **Following term name:** {insira aqui o número do seu semestre atual + 2}th semester
15. **Date classes start for the following term:** 08/14/2023
16. **Date exams end for the following term:** 12/22/2023
17. Caso daqui a dois semestres seja o seu último semestre na universidade, marque a caixa que contém **This is the last term in my degree**

O 9º passo é destinado a quem já **trabalha**/faz estágio, então responda de acordo com sua situação.

No 10º (e último) passo, selecione "From a classmate" caso tenha conhecido através da nossa mentoria do LKCAMP, ou outro que se aplique caso já tenha ouvido falar antes.

---
