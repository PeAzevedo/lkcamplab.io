---
title: "LKConf"
date: 2020-10-27T21:29:00-03:00
draft: false
---

Software livre move o mundo.
E as pessoas que tornam isso possível fazem com paixão.
E claro que por ser uma comunidade mundial, muitos desenvolvedores são do
Brasil.

Vamos conhecer melhor esses brasileiros que estão por trás dos projetos de
software livre.

## O que é?

O LKConf é uma série de encontros ocorrendo ao longo de novembro e dezembro de
2020 para conversar com desenvolvedores de software livre do Brasil e entender
melhor sobre a trajetória deles.

## Como participar

Todas as mesas redondas são transmitidas ao vivo para o
[Youtube](https://www.youtube.com/channel/UC2skRId4WWg9F0vc6GAKRIA).
Perguntas podem ser feitas através do chat ao vivo do Youtube ou pelo grupo do
[Telegram](https://t.me/lkcamp) ou canal do IRC (#lkcamp @ Freenode). Os vídeos
gravados estão também disponíveis no canal do LKCAMP no
[PeerTube](https://peertube.lhc.net.br/video-channels/lkcamp_lkconf/videos).

Além disso, todos os encontros também estão disponíveis como podcast. Para
ouví-los, adicione https://lkcamp.gitlab.io/lkconf_podcast/feed.xml ao seu
tocador de podcast, ou então ouça pelo
[Spotify](https://open.spotify.com/show/6S8K85VToF0K8f6zqE9LTc). Boas opções
de tocadores de podcast software livre são o
[AntennaPod](https://antennapod.org/) para Android, e o
[Vocal](https://vocalproject.net/) para desktop. Caso queira ouvir de alguma
outra maneira, os arquivos mp3 podem ser encontrados em
https://gitlab.com/lkcamp/lkconf_podcast/-/tree/master.

## Agenda

# 3 Nov (3ª), 19h15: Desenvolvedores de distros ([Gravação](https://www.youtube.com/watch?v=YZvrrG9BSlg))
* Giancarlo Razzolini (grazzolini) - Arch Linux
    * Entusiasta do software livre que contribui para diversos projetos há mais
      de 20 anos. Atualmente é desenvolver no Arch Linux, sendo mantenedor do
      mkinitcpio e também é parte do time de DevOps.
* Sergio Durigan Junior (sergiodj) - Debian
    * Debian Developer, mantenedor de pacotes no Fedora, desenvolvedor GDB e
      ativista de Software Livre. Trabalhou na Red Hat por 10 anos; atualmente
      faz parte do time de Ubuntu Server da Canonical.
# ~~10~~ 17 Nov (3ª), 19h15: Desenvolvedores do GNOME ([Gravação](https://www.youtube.com/watch?v=wqAFqbM7Pww))
* Clarissa Borges (clarissalimab)
    * Estudante de Engenharia de Software na Universidade de Brasília,
      participou no programa Outreachy 2018 e GSoc 2020, trabalhando com testes
      de usabilidade no GNOME. Recebeu o "Award for Outstanding New Free
      Software Contributor" da FSF.
* Georges Stavracas (feaneron)
    * Contribuidor do projeto GNOME, mantenedor de diversos programas,
      desenvolvedor na Endless Foundation e músico hobbista nas horas vagas.
* Rafael Fontenelle (rafaelff)
    * Apaixonado por FOSS, contribui principalmente com a tradução em diversos
      projetos (incluindo Python Docs, GNOME, Fedora, ArchWiki, bash, coreutils
      e outras ferramentas do GNU). Ama scripts, sempre criando-os e
      melhorando-os para facilitar as tarefas diárias.
# 24 Nov (3ª), 19h15: Participantes do Outreachy ([Gravação](https://www.youtube.com/watch?v=UF3qatE5xKg))
* Anna e só (contraexemplo)
    * Organizadora do programa de estágios em projetos de software livre
      Outreachy e mantenedora da documentação técnica do Open Collective.
      Atualmente, ela é graduanda em Sistemas de Informação pela Universidade
      Federal de Goiás.
      Anna já atuou em projetos de software livre brasileiros como Tainacan e
      Mapas Culturais em seu tempo trabalhando no MediaLab/UFG. Ela também foi
      consultora técnica no LAPPIS/UnB durante o processo de tradução da
      campanha "Dinheiro Público Código Público" da Free Software Foundation
      Europe. Nas horas vagas, ela frequentemente inventa diferentes maneiras de
      quebrar as suas instalações de GNU/Linux.
* Clarissa Borges (clarissalimab)
    * Estudante de Engenharia de Software na Universidade de Brasília,
      participou no programa Outreachy 2018 e GSoc 2020, trabalhando com testes
      de usabilidade no GNOME. Recebeu o "Award for Outstanding New Free
      Software Contributor" da FSF.
* Gabriela Bittencourt (gbittencourt)
    * Estudante de Engenharia da Computação na UNICAMP em duplo-diploma na
      Universidade Télécom-Paris, participou do Outreachy 2019 no driver VKMS do
      Linux Kernel - driver virtual da lib-DRM. Iniciando sua carreira em
      software livre no grupo LKCAMP em 2019, hoje em dia se considera uma
      apoiadora do movimento.
# ~~01~~ 08 Dez (3ª), 19h15: Desenvolvedores do KDE ([Gravação](https://www.youtube.com/watch?v=U6SKpjhm-Lg))
* Caio Jordão Carvalho (cdelimacarvalho)
    * Contribuidor da comunidade KDE desde o final de 2017, onde faz
      colaborações de código para diferentes aplicações da comunidade.
      Atualmente é mantenedor do marK, uma ferramenta voltada para construção de
      datasets de machine learning. Participou como mentor e estudante em
      diferentes edições do Google Summer of Code e Season of KDE. Fora isso,
      trabalha como pesquisador na área de inteligência artificial e computação
      de alto desempenho.
* Lucas Biaggi (lbiaggi)
    * Profissional de segurança da informação, trabalha com threat intel,
      cursando mestrado na UNESP em CC e no tempo livre ajuda nos projetos do
      KDE.
* Tomaz Canabrava (tcanabrava)
    * Desenvolvedor do konsole, atua com software livre pelos últimos 15 anos e
      é o responsável por diversos a programas do kde - nas horas vagas faz
      sushi e origami.
# 15 Dez (3ª), 19h15: Desenvolvedores do Kernel ([Gravação](https://www.youtube.com/watch?v=RKfr-5o8_wY))
* Gustavo Padovan (padovan)
    * Bacharel em Ciência da Computação pela Unicamp, onde foi membro ativo do
      Grupo Pró Software Livre (GPSL) e co-criador da disciplina de Seminários
      de Software Livre. Desenvolvedor do Kernel do Linux por mais de 10 anos.
      Sócio e líder técnico-estratégico na Collabora, uma das principais
      consultorias de Open Source do mundo. É também co-fundador da linuxdev-br
      conference, principal evento de desenvolvimento Linux e Open Source da
      América do Sul. Ministrou palestras em várias conferências internacionais
      de Linux e Open Source.
* Helen Koike (koike)
    * Engenheira de Software Senior e desenvolvedora de Kernel no time de kernel
      da Collabora. Recentemente trabalhou no driver Rockchip ISP1 no subsistema
      de Video4Linux media. Também já contribuiu em outras áreas do kernel,
      incluindo ASoC, device mapping, NVMe, mantém o driver Virtual Media
      Controller de um estágio passado no Outreachy e recentemente se tornou
      co-coordenadora do projeto Linux Kernel no Outreachy. Helen é formada em
      Ciência da Computação pela Universidade de Campinas (Unicamp), além de ter
      título de Engenheira pela Telecom Paristech e Universidade Pierre e Marie
      Curie.
* Rodrigo Siqueira (siqueira)
    * Formado em engenharia de software pela universidade de Brasília (Campus
      Gama) e tem mestrado em Ciência da Computação pela Universidade de São
      Paulo (IME). Atualmente ocupa a posição de Engenheiro de software Sr. na
      AMD (Canada), trabalhando ativamente com o driver de display do Kernel
      Linux e auxiliando com o processo de upstream de código para a comunidade.
      Siqueira é um dos mantenedores do VKMS, além de manter e atualizar o
      kworkflow (ferramenta que automatiza parte do fluxo de trabalho com
      kernel). Para mais informações visite siqueira.tech

## Agradecimentos
* Ao [LHC](https://lhc.net.br) por gentilmente ceder sua [instância do
  PeerTube](https://peertube.lhc.net.br/) para disponibilizarmos os vídeos em
  uma plataforma livre.
* A [StreamBeats](https://www.streambeats.com/) pelas músicas usadas nos vídeos.
* A todas as pessoas que participaram do bate-papo.
* A todas as pessoas que acompanharam os vídeos e podcasts.
